#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

n = 1024
X = np.random.normal(0, 1, n)
Y = np.random.normal(0, 1, n)

print "X: %s" % X

plt.scatter(X, Y)
plt.show()
