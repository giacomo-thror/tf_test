#!/usr/bin/env python

import __future__
import sys

import numpy as np
import matplotlib.pyplot as plt

def sumof(x):
    return x / np.sum(x, axis=0)

def softmax(x):
    return np.exp(x) / np.sum(np.exp(x), axis=0)

m1 = np.arange(-2.0, 6.0)
print "m1:\n", m1

x = np.vstack([m1, np.ones_like(m1), 0.2 * np.ones_like(m1)])

y = softmax(x)

print "x:\n", x
print "y:\n", y

print "sofmax sum:\n", np.sum(y, axis=0)


# print "test:\n", np.exp([1,2])
#!/usr/bin/env python
